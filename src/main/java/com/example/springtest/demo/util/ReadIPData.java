package com.example.springtest.demo.util;

import com.example.springtest.demo.bean.IPDataBean;
import org.springframework.util.ResourceUtils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReadIPData {

    private List<IPDataBean> ipList;

    private ReadIPData() {
    }

    private static class SingleHolder {
        public static final ReadIPData INSTANCE = new ReadIPData();
    }

    public static ReadIPData getInstance() {
        return SingleHolder.INSTANCE;
    }

    public String getCountryByIP(String ip) {
        Long requestIp = ipString2Long(ip);
        initIPData();
        long selectStartTime = System.currentTimeMillis();
//        long maxIp = 0;
//        for (int i = 0; i < ipList.size(); i++) {
//            IPDataBean ipDataBean = ipList.get(i);
//            long startIp = ipDataBean.getStartIp();
//            if (startIp > maxIp) {
//                maxIp = startIp;
//            }else{
//                System.out.println("找到一个比前面小的IP:" + startIp);
//            }
//            if (requestIp >= startIp && requestIp <= ipDataBean.getEndIp()) {
//                System.out.println("查询结果耗时：" + (System.currentTimeMillis() - selectStartTime));
//                return ipDataBean.getCountry();
//            }
//        }
        String result = binarySearch(requestIp);
        System.out.println("二分查询结果耗时：" + (System.currentTimeMillis() - selectStartTime));
        return result;
    }

    public void initIPData() {
        if (ipList != null) {
            return;
        }
        ipList = new ArrayList<>();
        long initStartTime = System.currentTimeMillis();
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        try {
            String csvFile = ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX + "static/csv/ip2country.CSV").getAbsolutePath();
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                try {
                    String[] splitIP = line.replaceAll("\"", "").split(cvsSplitBy);
//                    System.out.println("Country [ip= " + splitIP[0] + " , splitIP=" + splitIP[2] + "]");
                    IPDataBean ipDataBean = new IPDataBean();
                    ipDataBean.setStartIp(Long.parseLong(splitIP[0]));
                    ipDataBean.setEndIp(Long.parseLong(splitIP[1]));
                    ipDataBean.setCode(splitIP[2]);
                    ipDataBean.setCountry(splitIP[3]);
                    ipList.add(ipDataBean);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("读取文件耗时：" + (System.currentTimeMillis() - initStartTime));
        }
    }

    private Long ipString2Long(String ip) {
        Long ips = 0L;
        try {
            String[] numbers = ip.split("\\.");
            //等价上面
            for (int i = 0; i < 4; ++i) {
                ips = ips << 8 | Integer.parseInt(numbers[i]);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return ips;
    }


    /**
     * 二分查找优化
     * @param requestIP
     * @return
     */
    private String binarySearch(long requestIP) {
        if (ipList == null || ipList.size() <=0) {
            return "";
        }
        int low = 0;
        int height = ipList.size() - 1;
        int middle = 0;
        IPDataBean lowBean = ipList.get(low);
        IPDataBean heightBean = ipList.get(height);
        if (requestIP < lowBean.getStartIp() || requestIP > heightBean.getEndIp() || low > height) {
            return "";
        }
        while (low <= height) {
            middle = (low + height) / 2;
            IPDataBean middleBean = ipList.get(middle);
            if (middleBean.getStartIp() > requestIP) {
                //最小的还比要查的大，要查的在左边区域
                height = middle - 1;
            } else if (middleBean.getEndIp() < requestIP) {
                //最大的还比要查的小，要查的在右边区域
                low = middle + 1;
            } else {
                return middleBean.getCountry();
            }
        }
        return "";
    }
}
