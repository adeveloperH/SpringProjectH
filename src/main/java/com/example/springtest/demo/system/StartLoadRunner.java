package com.example.springtest.demo.system;

import com.example.springtest.demo.util.ReadIPData;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class StartLoadRunner implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        System.out.println("在服务启动时进行加载相应数据");
        ReadIPData.getInstance().initIPData();
    }
}
