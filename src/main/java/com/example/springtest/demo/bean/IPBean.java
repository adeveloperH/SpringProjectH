package com.example.springtest.demo.bean;

import java.util.Date;

public class IPBean {

    /**
     * as : AS25820 IT7 Networks Inc
     * city : Los Angeles
     * country : United States
     * countryCode : US
     * isp : IT7 Networks
     * lat : 34.0494
     * lon : -118.2641
     * org : IT7 Networks
     * query : 23.105.211.62
     * region : CA
     * regionName : California
     * status : success
     * timezone : America/Los_Angeles
     * zip : 90014
     */

    private String country;
    private String countryCode;
    private String query;
    private String status;
    private Date dataTime;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDataTime() {
        return dataTime;
    }

    public void setDataTime(Date dataTime) {
        this.dataTime = dataTime;
    }
}
