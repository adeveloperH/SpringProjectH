package com.example.springtest.demo.controller;

import com.example.springtest.demo.bean.ResponseBean;
import com.example.springtest.demo.util.ReadIPData;
import org.springframework.web.bind.annotation.*;

@RestController
public class IPController {

    //路径中获取参数。GET
//    @RequestMapping(path = "/myip/{ip}", method = RequestMethod.GET)
//    public String getIp(@PathVariable("ip") String requestIp) {
//        return "您的IP是：" + requestIp;
//    }

    //RequestParam 获取参数。支持 GET 和 POST
    @RequestMapping(path = "/myip", method = {RequestMethod.GET, RequestMethod.POST})
//    @PostMapping(path = "/myip")
    public ResponseBean getIP(@RequestParam("ip") String requestIp) {
        String countryByIP = ReadIPData.getInstance().getCountryByIP(requestIp);
        ResponseBean<String> response = new ResponseBean<>();
        response.setCode("200");
        response.setMsg("successed");
        response.setData("您的IP所在地是：" + countryByIP);
        return response;
    }

    @RequestMapping(path = "/exception", method = {RequestMethod.GET, RequestMethod.POST})
//    @PostMapping(path = "/myip")
    public void testException() {
        ResponseBean responseBean = null;
        responseBean.setData("");
    }


    //header 添加 content-type:json；Body 中使用 raw：{"ip":"123.126.111.131"}
//    @PostMapping(path = "/myip")
//    public String getIP(@RequestBody RequestIPBean requestIPBean) {
//        return "您的IP是：" + requestIPBean.getIp();
//    }


    //post 请求，表单提交 Body:form-data
//    @PostMapping(path = "/myip")
//    public String getIP(RequestIPBean requestIPBean) {
//        return "您的IP是：" + requestIPBean.getIp();
//    }


//    @RequestMapping(path = "/myip", method = {RequestMethod.GET, RequestMethod.POST})
//    public String getIp2(HttpServletRequest request) {
//        String requestIp = request.getParameter("ip");
//        return "您的IP是：" + requestIp;
//    }
}
