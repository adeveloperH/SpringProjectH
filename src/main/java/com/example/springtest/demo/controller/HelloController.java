package com.example.springtest.demo.controller;

import com.example.springtest.demo.bean.HelloBean;
import com.example.springtest.demo.bean.IPBean;
import com.google.gson.Gson;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

@RestController
//@EnableAutoConfiguration
public class HelloController {

    @RequestMapping("/hello")
    public HelloBean helloWorld() {
        HelloBean helloBean = new HelloBean();
        helloBean.setStatus(200);
        helloBean.setMsg("Hello Spring");
        return helloBean;
    }

    @RequestMapping(value = "/getip", method = RequestMethod.GET)
    public IPBean getIp(@RequestParam("ip") String ip) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.getForEntity("http://ip-api.com/json/" + ip, String.class);
        String responStr = responseEntity.getBody();
        IPBean ipBean = new Gson().fromJson(responStr, IPBean.class);
        ipBean.setDataTime(new Date());
        return ipBean;

    }
}
