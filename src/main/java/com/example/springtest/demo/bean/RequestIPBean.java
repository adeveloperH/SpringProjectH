package com.example.springtest.demo.bean;

public class RequestIPBean {
    private String ip;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
