package com.example.springtest.demo.system;

import com.example.springtest.demo.bean.ResponseBean;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.io.StringWriter;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResponseBean<String> defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
        ResponseBean<String> responseBean = new ResponseBean<>();
        responseBean.setCode("10001");
        responseBean.setMsg(getStackTrace(e));
        responseBean.setData(req.getRequestURI().toString());
        return responseBean;
    }

    public static String getStackTrace(Throwable throwable) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        try {
            throwable.printStackTrace(pw);
            return sw.toString();
        } finally {
            pw.close();
        }
    }
}
